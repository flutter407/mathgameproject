import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:game_project/class/quiz..dart';
import './game.dart';

class Choose extends StatefulWidget {
  const Choose({super.key});

  @override
  State<Choose> createState() => _Choose();
}

class _Choose extends State<Choose> {
  bool isHome = false;

  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var height = size.height / 2;
    return SafeArea(
        child: Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage("assets/images/choose.jpg"),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: isHome
            ? Text("Quiz Paused", style: TextStyle(fontSize: 24.0))
            : ListView(
                padding:  EdgeInsets.fromLTRB(5, 10,  height >= 320 ? 2 : 30, 20),
                children: [
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  child: FloatingActionButton(
                                backgroundColor: Colors.transparent,
                                child: Icon(
                                  Icons.home,
                                  color: Color.fromARGB(255, 255, 253, 253),
                                  size: 40.0,
                                ),
                                onPressed: () {
                                  setState(() {
                                    Navigator.of(context).pop();
                                    print(isHome);
                                  });
                                },
                              ))
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Container(
                                width: 140,
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(255, 97, 128, 172),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: height >= 320 ? height*0.1 : height*0.01),
                  Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                    Column(
                      children: [
                        buildlevel(
                          'บทที่ 1 การบวก',
                        ),
                        SizedBox(
                          height: 30,
                          width: 10,
                        ),
                        buildlevel('บทที่ 2 การลบ'),
                         SizedBox(
                          height: 30,
                          width: 10,
                        ),
                        buildlevel('บทที่ 3 การคูณ'),
                         SizedBox(
                          height: 30,
                          width: 10,
                        ),
                        buildlevel('บทที่ 4 การหาร')
                      ],
                    ),
                  ])
                ],
              ),
      ),
    ));
  }

  @override
  Widget buildlevel(String level) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          textStyle: const TextStyle(fontSize: 25),
          fixedSize: Size(200, 80),
          backgroundColor: Colors.orangeAccent,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        ),
        onPressed: () {
          switch (level) {
            case 'บทที่ 1 การบวก':
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const Game(
                          parameter: "+",
                        )),
              );
              break;
            case 'บทที่ 2 การลบ':
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const Game(
                          parameter: "-",
                        )),
              );
              break;
            case 'บทที่ 3 การคูณ':
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const Game(
                          parameter: "*",
                        )),
              );
              break;
            case 'บทที่ 4 การหาร':
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const Game(
                          parameter: "/",
                        )),
              );
              break;
            default:
          }
        },
        child: Text(
          '$level',
          style: TextStyle(color: Colors.indigo),
        ),
      ),
    );
  }
}
