import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:google_fonts/google_fonts.dart';
import './homePage.dart';
import './game.dart';

class LoseGame extends StatefulWidget {
  final String parameter;
  const LoseGame({Key? key, required this.parameter}) : super(key: key);

  @override
  State<LoseGame> createState() => _LoseGame();
}

@override
class _LoseGame extends State<LoseGame> {
  late String x = widget.parameter;
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    var size = MediaQuery.of(context).size;
    var height = size.height / 2;
    return OrientationBuilder(builder: (context, orientation) {
      return isPortrait
          ? SafeArea(
              child: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/result.png"),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Scaffold(
                  backgroundColor: Colors.transparent,
                  body: verticalBody(height),
                ),
              ),
            )
          : SafeArea(
              child: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/Result2.png"),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Scaffold(
                  backgroundColor: Colors.transparent,
                  body: horizontalBody(height),
                ),
              ),
            );
    });
  }

  Widget verticalBody(double height) {
    const colorizeColors = [
      Colors.white,
      Colors.blueGrey,
      Colors.lightBlueAccent,
      Colors.blueAccent,
    ];

    var colorizeColors2 = [
      Colors.white,
      Colors.orange.shade100,
      Colors.orange.shade300,
      Colors.orange.shade500,
    ];

    // const colorizeTextStyle = TextStyle(
    //   fontSize: 50.0,
    // );

    return ListView(children: [
      Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, height / 2 - 100, 0, 0),
                    child: Center(
                      child: Container(
                        child: AnimatedTextKit(
                            repeatForever: true,
                            animatedTexts: [
                              ColorizeAnimatedText(
                                'ตั้งใจเรียนบ้างนะ ',
                                textStyle: TextStyle(
                                  fontSize: 60.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'MN Bologna',
                                ),
                                colors: colorizeColors,
                              ),
                            ],
                            isRepeatingAnimation: true),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                    child: Center(
                      child: Container(
                        child: AnimatedTextKit(
                            repeatForever: true,
                            animatedTexts: [
                              ColorizeAnimatedText(
                                'แพ้แล้ว จงอยู่ที่นี้ต่อไป',
                                textStyle: TextStyle(
                                  fontSize: 35.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'MN Bologna',
                                ),
                                colors: colorizeColors2,
                              ),
                            ],
                            isRepeatingAnimation: true),
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(0),
                            child: Image(
                              image: AssetImage("assets/images/wizard.png"),
                              height: 300,
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 60, 0, 60),
                            child: Column(children: [
                              FloatingActionButton.large(
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>  Game(parameter: x,)),
                                  );
                                },
                                child: Icon(
                                  Icons.refresh,
                                  size: 50,
                                  color: Color.fromARGB(255, 240, 175, 54),
                                ),
                                backgroundColor: Colors.white,
                              ),
                              SizedBox(
                                height: 20,
                                width: 20,
                              ),
                              FloatingActionButton.large(
                                child: Icon(Icons.exit_to_app),
                                backgroundColor: Colors.white,
                                foregroundColor: Colors.blue,
                                onPressed: () => {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => const HomePage()),
                                  ),
                                },
                              ),
                            ]),
                          ),
                        ],
                      ),
                      // Padding(
                      //   padding: EdgeInsets.all(0),
                      //   child: Image(
                      //     image: AssetImage("assets/images/wizard.png"),
                      //     height: 100,
                      //     fit: BoxFit.fitHeight,
                      //   ),
                      // ),
                    ],
                  ),
                  // Padding(
                  //   padding: EdgeInsets.fromLTRB(0, 20, 0, 60),
                  //   child: Column(children: [
                  //     FloatingActionButton.large(
                  //       onPressed: () {
                  //         Navigator.push(
                  //           context,
                  //           MaterialPageRoute(
                  //               builder: (context) => const Game()),
                  //         );
                  //       },
                  //       child: Icon(
                  //         Icons.refresh,
                  //         size: 50,
                  //         color: Color.fromARGB(255, 240, 175, 54),
                  //       ),
                  //       backgroundColor: Colors.white,
                  //     ),
                  //     SizedBox(
                  //       height: 20,
                  //       width: 20,
                  //     ),
                  //     FloatingActionButton.large(
                  //       child: Icon(Icons.exit_to_app),
                  //       backgroundColor: Colors.white,
                  //       foregroundColor: Colors.blue,
                  //       onPressed: () => {
                  //         Navigator.push(
                  //           context,
                  //           MaterialPageRoute(
                  //               builder: (context) => const HomePage()),
                  //         ),
                  //       },
                  //     ),
                  //   ]),
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    ]);
  }

  Widget horizontalBody(double height)  {
    const colorizeColors = [
      Colors.white,
      Colors.blueGrey,
      Colors.lightBlueAccent,
      Colors.blueAccent,
    ];

    var colorizeColors2 = [
      Colors.white,
      Colors.orange.shade100,
      Colors.orange.shade300,
      Colors.orange.shade500,
    ];


    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Center(
            child: Container(
              
              child: AnimatedTextKit(
                  repeatForever: true,
                  animatedTexts: [
                    ColorizeAnimatedText(
                      'ตั้งใจเรียนบ้างนะ',
                      textStyle: TextStyle(
                        fontSize: 50.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'MN Bologna',
                      ),
                      colors: colorizeColors,
                    ),
                  ],
                  isRepeatingAnimation: true),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 30),
          child: Center(
            child: Container(
              child: AnimatedTextKit(
                  repeatForever: true,
                  animatedTexts: [
                    ColorizeAnimatedText(
                      'แพ้แล้ว จงอยู่ที่นี้ต่อไป',
                      textStyle: TextStyle(
                        fontSize: 35.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'MN Bologna',
                      ),
                      colors: colorizeColors2,
                    ),
                  ],
                  isRepeatingAnimation: true),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(0),
              child: Image(
                image: AssetImage("assets/images/wizard.png"),
                height: 140,
                fit: BoxFit.fitHeight,
              ),
            ),
            SizedBox(
              width: 70,
            ),
            FloatingActionButton.large(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>  Game(parameter: x,)),
                );
              },
              child: Icon(
                Icons.refresh,
                size: 50,
                color: Color.fromARGB(255, 240, 175, 54),
              ),
              backgroundColor: Colors.white,
            ),
            SizedBox(
              width: 70,
            ),
            FloatingActionButton.large(
              child: Icon(
                Icons.exit_to_app,
                size: 40,
              ),
              backgroundColor: Colors.white,
              foregroundColor: Colors.blue,
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomePage()),
                ),
              },
            ),
          ],
        ),
      ],
    ));
  }
}
