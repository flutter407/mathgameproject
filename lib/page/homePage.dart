import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:game_project/page/choose.dart';
import 'package:google_fonts/google_fonts.dart';
import './game.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var height = size.height / 2;
    return SafeArea(
      child: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/home2.png"),
            fit: BoxFit.fill,
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: verticalBody(height),
        ),
      ),
    );
  }

  Widget horizontalBody(double height) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: height / 2 + 100,
          ),
          FloatingActionButton.large(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Choose()),
              );
            },
            child: Icon(
              Icons.play_arrow,
              size: 50,
              color: Color.fromARGB(255, 240, 175, 54),
            ),
            backgroundColor: Colors.white,
          ),
          SizedBox(
            width: 70,
          ),
          FloatingActionButton.large(
            child: Icon(
              Icons.exit_to_app,
              size: 40,
            ),
            backgroundColor: Colors.white,
            foregroundColor: Colors.blue,
            onPressed: () => {},
          ),
        ],
      ),
    );
  }

  Widget verticalBody(double height) {
    const colorizeColors = [
      Colors.white,
      Colors.grey,
      Colors.white,
      Colors.black,
      Colors.white,
    ];

    const colorizeColors2 = [
      Colors.orangeAccent,
      Colors.white,
      Color.fromARGB(255, 240, 175, 54),
      Color.fromARGB(255, 228, 228, 228),
      Color.fromARGB(255, 240, 175, 54),
    ];

    const colorizeTextStyle = TextStyle(
      fontSize: 50.0,
    );

    return ListView(children: [
      Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, height / 2 - 50, 0, 0),
                    child: Center(
                      child: Container(

                        child: AnimatedTextKit(
                            repeatForever: true,
                            animatedTexts: [
                              ColorizeAnimatedText(
                                'Math For Life',
                                textStyle: TextStyle(
                                  fontSize: 54.0,
                                  fontWeight: FontWeight.bold,
                                    fontFamily: 'ShantellSans',
                                ),
                                colors: colorizeColors,
                              ),
                            ],
                            isRepeatingAnimation: true),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: Center(
                      child: Container(
                        color: Color.fromARGB(255, 61, 61, 61).withOpacity(0.1),
                        child: AnimatedTextKit(
                          repeatForever: true,
                          animatedTexts: [
                            ColorizeAnimatedText(
                              'Go back',
                              textStyle: TextStyle(
                                fontSize: 44.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'ShantellSans'),
                              colors: colorizeColors2,
                            ),
                            ColorizeAnimatedText(
                              'Or',

                              textStyle: TextStyle(
                                  fontSize: 44.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'ShantellSans'),
                              colors: colorizeColors2,
                            ),
                            ColorizeAnimatedText(
                              'Stay here',

                              textStyle: TextStyle(
                                  fontSize: 44.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'ShantellSans'),
                              colors: colorizeColors2,
                            ),
                            ColorizeAnimatedText(
                              'Forever..',
                              textStyle: TextStyle(
                                  fontSize: 44.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'ShantellSans'),
                              colors: colorizeColors2,
                            ),
                          ],
                          isRepeatingAnimation: true,
                        ),
                      ),
                    ),
                  ),
                  height >= 320
                      ? Container(
                          padding:
                              EdgeInsets.fromLTRB(0, (height / 2) - 50, 0, 0),
                          child: Column(children: [
                            FloatingActionButton.large(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const Choose()),
                                );
                              },
                              child: Icon(
                                Icons.play_arrow,
                                size: 50,
                                color: Color.fromARGB(255, 240, 175, 54),
                              ),
                              backgroundColor: Colors.white,
                            ),
                            SizedBox(
                              height: 30,
                              width: 20,
                            ),
                            FloatingActionButton.large(
                              child: Icon(Icons.exit_to_app),
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.blue,
                              onPressed: () => {},
                            ),
                          ]),
                        )
                      : horizontalBody(height),
                ],
              ),
            ),
          ],
        ),
      ),
    ]);
  }
}
