import 'package:flutter/material.dart';

class Quiz extends StatefulWidget {
  @override
  _QuizState createState() => _QuizState();
}

class _QuizState extends State<Quiz> {
  bool isPaused = false;
  int currentIndex = 0;
  int score = 0;
  List<Question> questions = [    Question(      "What is the capital of France?",      ["Paris", "London", "Berlin", "Madrid"],
      "Paris",
    ),
    // add more questions here
  ];

  // display the quiz question and handle user answers
  Widget buildQuizQuestion() {
    return QuizQuestion(
      question: questions[currentIndex],
      onAnswerSelected: (bool isCorrect) {
        setState(() {
          if (isCorrect) {
            score++;
          }
          if (currentIndex < questions.length - 1) {
            currentIndex++;
          } else {
            showResultDialog();
          }
        });
      },
    );
  }

  // display the result dialog after the quiz is completed
  void showResultDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Quiz Completed"),
          content: Text("Your score is $score"),
          actions: [
            ElevatedButton(
              child: Text("Restart"),
              onPressed: () {
                Navigator.of(context).pop();
                restartQuiz();
              },
            ),
            ElevatedButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  // restart the quiz
  void restartQuiz() {
    setState(() {
      currentIndex = 0;
      score = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Quiz"),
        actions: [
          ElevatedButton(
            child: Text(isPaused ? "Resume" : "Pause"),
            onPressed: () {
              setState(() {
                isPaused = !isPaused;
              });
            },
          )
        ],
      ),
      body: Center(
        child: isPaused
            ? Text("Quiz Paused", style: TextStyle(fontSize: 24.0))
            : buildQuizQuestion(),
      ),
    );
  }
}

// define the Question class
class Question {
  String question;
  List<String> choices;
  String answer;

  Question(this.question, this.choices, this.answer);
}

// define the QuizQuestion widget
class QuizQuestion extends StatelessWidget {
  final Question question;
  final Function(bool) onAnswerSelected;

  QuizQuestion({required this.question, required this.onAnswerSelected});

  @override
  Widget build(BuildContext context) {
    // implement the quiz question UI
    return Text(question.question);
  }
}
