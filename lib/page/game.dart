import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:game_project/class/quiz..dart';
import 'dart:math';
import 'dart:async';
import 'result.dart';
import 'package:provider/provider.dart';

class Game extends StatefulWidget {
  
  final String parameter;
  const Game({Key? key, required this.parameter}) : super(key: key);

  @override
  State<Game> createState() => _Game();
}

class _Game extends State<Game> {
  
  late Questions questions;
  late String x = widget.parameter;
  // Questions createQuiz(String x) {
  //   return Questions(sign: x);
  // }

  late Timer _timer;
  int _start = 10;

  var countWin = 0;
  bool isPaused = false;
  double height = 0.0;
  var gameCount = 0;
  int heart = 3;
  int heartBroken = 0;
  bool correct = false;
  bool hideChoice = true;
  // bool showIcon = true;
  final audio = AssetsAudioPlayer();

  @override
  void initState() {
    super.initState();
    // start fetching
    startTimer();
    countWin = 0;
    hideChoice = true;
    questions = Questions(sign: x);

    questions.makeQuiz();
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            _start = 10;
            if (gameCount == 5 || heart == 0) {
              _start = -1;
              _timer.cancel();
            }
          });
          gameCount++;
          questions.makeQuiz();
          decreaseHeart();
          checkWin();
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  void checkWin() {
    if (gameCount == 5 && heart == 0) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => Result(
                  parameter: x,
                  check: 0,
                )),
      );
    } else if (heart == 0) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => Result(
                  parameter: x,
                  check: 0,
                )),
      );
    } else if (gameCount == 5 && heart != 0) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => Result(
                  parameter: x,
                  check: 1,
                )),
      );
    }
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Widget build(BuildContext context) {
    String strDigits(int n) => n.toString().padLeft(2, '0');
    final size = MediaQuery.of(context).size;
    final height2 = size.height;
    height = height2;

    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final Questions counter = Provider.of<Questions>(context);
    // counter
    return SafeArea(
        child: Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
        fit: BoxFit.fill,
        image: AssetImage("assets/images/star.png"),
      )),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: isPaused
            ? Text("Quiz Paused", style: TextStyle(fontSize: 24.0))
            : ListView(
                padding: const EdgeInsets.fromLTRB(5, 5, 5, 0),
                children: [
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  child: FloatingActionButton(
                                backgroundColor: Colors.transparent,
                                child: Icon(
                                  Icons.replay,
                                  color: Color.fromARGB(255, 255, 253, 253),
                                  size: 40.0,
                                ),
                                onPressed: hideChoice
                                    ? () {
                                        setState(() {
                                          _timer.cancel();
                                          hideChoice = true;
                                          _start = 10;
                                          gameCount = 0;
                                          countWin = 0;
                                          questions = counter;
                                          // for (int i = 0; i < 6; i++) {
                                          //   questions.add(createQuiz(x));
                                          // }
                                          questions.makeQuiz();
                                          heart = 3;
                                          startTimer();
                                          print(isPaused);
                                        });
                                      }
                                    : null,
                              ))
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Container(
                                width: 140,
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(255, 97, 128, 172),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    for (int i = 0; i < 3; i++)
                                      i < heart
                                          ? buildheart()
                                          : buildheartBroke(),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(0),
                    child: Center(
                        child: Text('${_start}',
                            style: TextStyle(
                              fontSize: 30,
                              color: Colors.white,
                              fontFamily: 'ShantellSans',
                            ))),
                  ),
                  SizedBox(height: height > 500 ? 50 : 5),
                  Center(
                    child: ConstrainedBox(
                        constraints:
                            const BoxConstraints(maxHeight: 500, maxWidth: 500),
                        child: Container(
                          padding: EdgeInsets.all(8.0),
                          color: Colors.white.withOpacity(0.9),
                          width: 300,
                          height: 200,
                          child: Center(
                            child: Column(
                              children: [
                                Text(
                                  '${questions.firstNumber} ${questions.sign} ${questions.secondNumber} ',
                                  style: (TextStyle(
                                    fontSize: 44,
                                    fontFamily: 'ShantellSans',
                                  )),
                                ),
                                Text('=',
                                    style: (TextStyle(
                                      fontSize: 44,
                                      fontFamily: 'ShantellSans',
                                    ))),
                                Text(
                                    '${correct ? '${questions.takeAnswer}' : '?'}',
                                    style: (TextStyle(
                                      fontSize: 44,
                                      fontFamily: 'ShantellSans',
                                    )))
                              ],
                            ),
                          ),
                        )),
                  ),
                  SizedBox(height: height > 500 ? 50 : 5),
                  height > 500
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                              Column(
                                children: [
                                  if (questions != null)
                                    for (var choice in questions.choices)
                                      buildChoice(choice),
                                ],
                              )
                            ])
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                              if (questions != null)
                                for (var choice in questions.choices)
                                  buildChoice(choice),
                            ]),
                ],
              ),
      ),
    ));
  }

  void decreaseHeart() {
    setState(() {
      if (heart == 0) {
        return;
      }
      heart--;
      heartBroken++;
    });
  }

  @override
  Widget buildheart() {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        children: <Widget>[
          Icon(
            Icons.favorite_outlined,
            color: Color.fromARGB(255, 221, 7, 0),
            size: 30,
          ),
        ],
      ),
    );
  }

  @override
  Widget buildheartBroke() {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        children: <Widget>[
          Icon(
            Icons.heart_broken,
            color: Colors.white,
            size: 30,
          ),
        ],
      ),
    );
  }

  @override
  Widget buildChoice(var answer) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          textStyle: const TextStyle(
            fontSize: 20,
            fontFamily: 'ShantellSans',
          ),
          fixedSize: Size(140, 40),
          backgroundColor: Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        ),
        onPressed: hideChoice
            ? () {
                setState(() {
                  _timer.cancel();
                  gameCount++;
                  if (answer != questions.takeAnswer) {
                    decreaseHeart();
                  } else {
                    countWin++;
                  }
                  ;
                  // if (questions.length == 1) {
                  //   Navigator.pop(context);
                  // }

                  // questions.remove(questions[1]);

                  if (heart == 0 || gameCount == 5) {
                    correct = true;
                    Future.delayed(const Duration(milliseconds: 2000), () {
                      setState(() {
                        correct = false;
                        checkWin();
                      });
                    });
                  } else {
                    hideChoice = false;
                    correct = true;
                    Future.delayed(const Duration(milliseconds: 2000), () {
                      setState(() {
                        _start = 10;
                        startTimer();
                        hideChoice = true;
                        correct = false;
                        questions.makeQuiz();
                      });
                    });
                  }
                  ;

                  // print(questions.length);
                });
              }
            : null,
        child: Text(
          '${answer}',
          style: TextStyle(color: Colors.black),
        ),
      ),
    );
  }
}
