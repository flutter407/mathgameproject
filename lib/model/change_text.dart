import 'package:flutter/material.dart';
import 'dart:math';

class ChangeText with ChangeNotifier{
  String textTop = "";
  String textBelow = "";
  void changeChecWinLose(int check) {
    // 0 แพ้, 1 ชนะ
    if(check == 0){
      textTop="ตั้งใจเรียนบ้างนะ";
      textBelow= "แพ้แล้ว จงอยู่ที่นี้ต่อไป";
    }else if (check ==1){
      textTop="เก่งมากเจ้าเด็กน้อย";
      textBelow= "ชนะแล้ว กลับบ้านได้";
    };
    notifyListeners();
  }
}