import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:simple_grid/simple_grid.dart';
import './page/game.dart';
import './page/test.dart';
import './page/homePage.dart';
import './page/losegame.dart';
import 'page/result.dart';
import 'package:provider/provider.dart';
import 'package:game_project/class/quiz..dart';
import 'model/change_text.dart';
import 'class/quiz..dart';

// import 'package:assets_audio_player/assets_audio_player.dart';
void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(
      create: (context) => ChangeText(),
    ),
     ChangeNotifierProvider(
      create: (context) => Questions(),
    ),
  ], child: MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  final assetAutdi = AssetsAudioPlayer();
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        title: 'Project game',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          fontFamily: 'ShantellSans',
        ),
        home: const HomePage(),
      ),
    );
  }
}
