import 'package:flutter/material.dart';
import 'dart:math';

class Questions with ChangeNotifier{
  // final String question;
  var answerA;
  var answerB;
  var answerC;
  var firstNumber = 0.0;
  var secondNumber = 0.0;
  var correctAnswer = 0.0;
  var takeAnswer ;
  Random _random = Random();
  var sign = "";
  List choices = [];

  Questions(
      {
      this.sign = ""});

  void makeQuiz() {
    switch (sign) {
      case "+":
        firstNumber = _random.nextInt(50) + 1;
        secondNumber = _random.nextInt(50) + 1;
        correctAnswer = firstNumber + secondNumber;
        answerA = _random.nextInt(10) + (correctAnswer + 2);
        answerB = _random.nextInt(5) + (answerA + 1);
        answerC = _random.nextInt(2) + (answerB + 2);
        takeAnswer = correctAnswer;
        choices = [answerA, answerB, answerC, correctAnswer.round()];
        choices.shuffle(Random());
        break;
      case "-":
        firstNumber = _random.nextInt(100) + 10;
        secondNumber = _random.nextInt(50) + 1;
        correctAnswer = firstNumber - secondNumber;
        answerA = _random.nextInt(10) + (correctAnswer + 2);
        answerB = _random.nextInt(5) + (answerA + 1);
        answerC = _random.nextInt(2) + (answerB + 2);
        takeAnswer = correctAnswer;
        choices = [answerA, answerB, answerC, correctAnswer.round()];
        choices.shuffle(Random());
        break;
      case "*":
        firstNumber = _random.nextInt(11) + 1;
        secondNumber = _random.nextInt(11) + 1;
        correctAnswer = firstNumber * secondNumber;
        answerA = _random.nextInt(2) + (correctAnswer + 1);
        answerB = _random.nextInt(5) + (answerA + 1);
        answerC = _random.nextInt(2) + (answerB + 2);
        takeAnswer = correctAnswer;
        choices = [answerA, answerB, answerC, correctAnswer.round()];
        choices.shuffle(Random());
        break;
      case "/":
        firstNumber = (_random.nextInt(11) + 1);
        secondNumber = (_random.nextInt(11) + 1);
        correctAnswer = double.parse((firstNumber / secondNumber).toStringAsFixed(2));
        answerA = (correctAnswer + 1 + _random.nextInt(2)).toDouble();
        answerB = (answerA + 1 + _random.nextInt(5)).toDouble();
        answerC = (answerB + 2 + _random.nextInt(2)).toDouble();
        takeAnswer = correctAnswer.toStringAsFixed(2);
        choices = [answerA.toStringAsFixed(2), answerB.toStringAsFixed(2), answerC.toStringAsFixed(2), correctAnswer.toStringAsFixed(2)];
        choices.shuffle(Random());
        break;
      default:
    }
     notifyListeners();
  }
}
